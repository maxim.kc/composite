package com.company.compartments;

import com.company.abstraction.iToolCompartment;
import com.company.tools.Tools;

public class NailCompartment implements iToolCompartment {

    Tools tool = Tools.NAILS;

    @Override
    public void printToolCompartmentAssignment() {
        System.out.println("This compartment is for :" + tool);
    }
}
