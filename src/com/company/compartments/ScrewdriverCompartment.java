package com.company.compartments;

import com.company.abstraction.iToolCompartment;
import com.company.tools.Tools;

public class ScrewdriverCompartment implements iToolCompartment {

    Tools tool = Tools.SCREWDRIVER;

    @Override
    public void printToolCompartmentAssignment() {
        System.out.println("This compartment is for :" + tool);
    }
}
