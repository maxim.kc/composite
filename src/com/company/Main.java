package com.company;

import com.company.compartments.HammerCompartment;
import com.company.compartments.NailCompartment;
import com.company.compartments.ScrewdriverCompartment;
import com.company.compartments.SpannerCompartment;

public class Main {

    public static void main(String[] args) {

        ToolBox toolBox = new ToolBox();
        toolBox.printToolCompartmentAssignment();
        toolBox.printEachInnerToolCompartmentAssignment();

        System.out.println();

        toolBox.addCompartment(new SpannerCompartment());
        toolBox.addCompartment(new ScrewdriverCompartment());
        toolBox.addCompartment(new HammerCompartment());
        toolBox.addCompartment(new NailCompartment());

        toolBox.printEachInnerToolCompartmentAssignment();
        System.out.println();

        toolBox.removeCompartment(toolBox.getToolCompartment(2));

        toolBox.printEachInnerToolCompartmentAssignment();

    }
}
